# Romanian translation of the Debian Release Notes
# Copyright (C) 2005-2019 the respective copyright holders:
# This file is distributed under the same license as the Debian Release Notes.
# Dan Damian <dand@codemonkey.ro>, 2005.
# Eddy Petrișor <eddy.petrisor@gmail.com>, 2006, 2007.
# Stan Ioan-Eugen <stan.ieugen@gmail.com>, 2006, 2007.
# Andrei Popescu <andreimpopescu@gmail.com>, 2007, 2008, 2009, 2010, 2013, 2019
# Igor Știrbu <igor.stirbu@gmail.com>, 2009.
# Daniel Șerbănescu <daniel [at] serbanescu [dot] dk>, 2019.
# Andrei POPESCU <andreimpopescu@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2019-07-10 08:45+0300\n"
"Last-Translator: Andrei POPESCU <andreimpopescu@gmail.com>\n"
"Language-Team: Romanian Team <debian-l10n-romanian@lists.debian.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2);;\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "en"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Gestionarea sistemului &oldreleasename; înainte de actualizare"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Această anexă conține informații despre cum să vă asigurați că puteți "
"instala sau actualiza pachete &oldreleasename; înainte de a actualiza la "
"&releasename;. Acest lucru ar trebui să fie necesar doar în anumite situații."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Actualizarea sistemului &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Practic aceasta nu este diferită de altă actualizare pe care ați efectuat-o "
"pentru &oldreleasename;. Singura diferență este că trebuie să vă asigurați "
"mai întâi că lista de pachete încă conține referințe către &oldreleasename;, "
"după cum se explică în <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Dacă actualizați sistemul folosind un sit-oglindă Debian, atunci sistemul va "
"fi actualizat automat la ultima versiune intermediară de &oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "Verificarea fișierelor listelor de surse APT"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Dacă oricare dintre liniile surselor APT (consultați <ulink url=\"&url-man;/"
"&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>) conțin "
"referințe la <quote><literal>stable</literal></quote>, înseamnă că acestea "
"se referă deja la &releasename;. Poate nu doriți acest lucru dacă nu ați "
"terminat pregătirile pentru actualizare. Dacă ați rulat deja <command>apt "
"update</command>, puteți încă da înapoi urmând procedurile de mai jos."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Dacă deja ați instalat pachete din &releasename;, probabil nu mai are rost "
"să mai instalați pachete din &oldreleasename;. În acest caz va trebui să "
"decideți dacă doriți să continuați sau nu. Este posibil să reveniți la "
"versiuni anterioare ale pachetelor, însă acest subiect nu este acoperit aici."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Folosind editorul preferat deschideți ca root fișierul relevant cu surse APT "
"(cum ar fi <filename>/etc/apt/sources.list</filename>) și verificați toate "
"liniile care încep cu <literal>deb http:</literal>, <literal>deb https:</"
"literal>, <literal>deb tor+http:</literal>, <literal>deb tor+https:</"
"literal>, <literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> sau <literal>URIs: tor+https:</literal> "
"pentru referințe la <quote><literal>stable</literal></quote>. Dacă există "
"modificați <literal>stable</literal> în <literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Dacă aveți linii care încep cu <literal>deb file:</literal>sau "
"<literal>URIs: file:</literal>, va trebui să verificați dacă locația la care "
"se referă conține o arhivă &oldreleasename; sau &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Nu modificați liniie care încep cu <literal>deb cdrom:</literal> sau "
"<literal>URIs: cdrom:</literal>. În felul acesta veți invalida linia și va "
"trebui să rulați din nou <command>apt-cdrom</command>. Nu vă alarmați dacă o "
"linie de surse <literal>cdrom:</literal> se referă la <literal>unstable</"
"literal>. Acest lucru este normal, deși poate crea confuzii."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Dacă ați efectuat modificări, salvați fișierul și executați"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "pentru a actualiza lista de pachete."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Îndepărtarea fișierelor de configurare inutile"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Înainte de a actualiza sistemul la &releasename; este recomandat să ștergeți "
"fișierele vechi de configurare (cum ar fi fișierele <filename>*.dpkg-{new,"
"old}</filename> din <filename>/etc</filename>) din sistem."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Actualizați localizările învechite la UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "Folosirea unei localizări de tip non-UTF-8 nu mai este suportată de mult "
#~ "timp de desktopuri și alte proiecte de programe uzuale. Asemenea "
#~ "localizări ar trebui să fie înnoite rulând <command>dpkg-reconfigure "
#~ "locales</command> și selectând o localizare implicită UTF-8. Ar trebui să "
#~ "vă asigurați și că utilizatorii nu suprascriu valoarea implicită pentru a "
#~ "folosi o localizare învechită în mediu lor."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "În protectorul de ecran GNOME folosirea caracterelor non-ASCII, suportul "
#~ "pam_ldap sau chiar și deblocarea ecranului nu sunt fiabile dacă nu se "
#~ "folosește UTF-8. Cititorul de ecran GNOME este afectat de problema <ulink "
#~ "url=\"http://bugs.debian.org/599197\">#599197</ulink>. Managerul de "
#~ "fișiere Nautilus (și toate programele bazate pe glib, și probabil și "
#~ "toate programele bazate pe QT) presupun că numele fișierelor sunt UTF-8, "
#~ "în timp ce interpretorul de comenzi presupune că sunt în codificarea "
#~ "localei curente. În utilizarea de zi cu zi numele de fișiere non-ASCII "
#~ "sunt inutilizabile în asemenea configurații. Mai mult, cititorul de ecran "
#~ "gnome-orca (care asigură accesul utilizatorilor nevăzători la mediul "
#~ "GNOME) necesită o locală UTF-8 începând cu Squeeze. Cu un set de "
#~ "caractere vechi va fi incapabil să citească informațiile ferestrelor "
#~ "pentru elemente cum ar fi Nautilus, GNOME Panel sau meniul Alt-F1."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Dacă sistemul dumneavoastră este localizat folosind o localizare care nu "
#~ "este bazată pe UTF-8 este important să configurați sistemul să folosească "
#~ "UTF-8. În trecut au fost raportate probleme<placeholder type=\"footnote\" "
#~ "id=\"0\"/> care se manifestă doar atunci când se folosește o localizare "
#~ "non-UTF-8. Pe un sistem de birou, asemenea localizări sunt suportate doar "
#~ "prin niște improvizații urâte în bibliotecile de sistem și nu putem oferi "
#~ "suport decent pentru utilizatorii care încă le mai folosesc."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "Pentru a configura localizarea sistemului puteți rula comanda "
#~ "<command>dpkg-reconfigure locales</command>. Asigurați-vă că alegeți o "
#~ "localizare UTF-8 atunci când vă este prezentată întrebarea despre "
#~ "localizarea implicită a sistemului (n.tr.: pentru limba română "
#~ "localizarea corectă este „ro_RO.UTF-8”). Suplimentar ar trebui să "
#~ "verificați setările de localizare a utilizatorilor de pe sistemul "
#~ "dumneavoastră ca să vă asigurați că nu este definită o localizare "
#~ "învechită în configurația lor."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "Începând cu versiunea 2:1.7.7-12 xorg-server nu mai citește fișierul "
#~ "XF86Config-4. Consultați și <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
