# Translation of about.po to Galician
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pablo <parodper@gmail.com>, 2021.
#
# Traductores:
#     Para esta traducción usei (Pablo) o dicionario do Proxecto Trasno (http://termos.trasno.gal/) e o DiGaTIC (http://www.digatic.org/gl)
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"POT-Creation-Date: 2021-04-20 10:44+0200\n"
"PO-Revision-Date: 2021-06-10 19:22+0200\n"
"Last-Translator: Pablo <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "gl"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Introdución"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"O obxectivo deste documento é informar aos usuarios da distribución &debian; "
"sobre os principais cambios na versión &release; (alcumada &releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"As notas da versión conteñen máis información sobre como se pode actualizar "
"de forma segura dende a versión &oldrelease; (alcumada &oldreleasename;) á "
"versión actual e informan aos usuarios dos posibles problemas que se sabe "
"poden ocorrer."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>."
msgstr ""
"Podes obter a última versión deste documento en <ulink url=\"&url-release-"
"notes;\"></ulink>."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:26
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Teña en conta que é imposible amosar todos os problemas que se coñecen, polo "
"que foi necesario facer unha selección baseándose na probabilidade de que "
"ocorran e o seu impacto."

#. type: Content of: <chapter><para>
#: en/about.dbk:32
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Lembre que só lle damos asistencia técnica para actualizar dende a versión "
"de Debian anterior (neste caso, actualizar dende &oldreleasename;).  Se "
"necesitas actualizar dende versións anteriores, suxerímoslle que lea as "
"edicións anteriores das notas da versión e actualice antes a "
"&oldreleasename;."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:40
msgid "Reporting bugs on this document"
msgstr "Avise de erros neste documentos"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:42
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Probamos todos os diferentes pasos descritos neste documento para realizar a "
"actualización e intentamos anticiparnos a todos os problemas que se poidan "
"atopar os nosos usuarios."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:47
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"De todas formas se vostede cre que atopou un fallo (información errónea ou "
"ausente)  nesta documentación, por favor abra unha petición no <ulink url="
"\"&url-bts;\">sistema de seguimento de fallos</ulink> sobre o paquete "
"<systemitem role=\"package\">release-notes</systemitem>. Revise os <ulink "
"url=\"&url-bts-rn;\">informes de fallos anteriores</ulink> en caso de que "
"alguén xa informara sobre o problema que atopou. Engada sen medo nova "
"información aos informes xa existentes se pode contribuír con contido para "
"este documento."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:59
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Agradecémoslle, e animámoslle a, que engada parches nas fontes do documento "
"xunto cos informes. Pode obter máis información de como obter as fontes "
"deste documento en <xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:67
msgid "Contributing upgrade reports"
msgstr "Colaborando con informes de actualización"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:69
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Toda colaboración por parte dos usuarios relacionada coas actualizacións "
"dende &oldreleasename; a &releasename; é benvida.  Se esta disposto a "
"compartir información por favor abra unha petición no <ulink url=\"&url-bts;"
"\">sistema de seguimento de fallos</ulink> sobre o paquete <systemitem role="
"\"package\">upgrade-reports</systemitem> coas súas achegas.  Pedímoslle que "
"comprima todos os ficheiros que engada (usando <command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:78
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Por favor inclúa a seguinte información cando envíe o seu informe de "
"actualización:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:85
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"O estado da súa base de datos de paquetes antes e despois da actualización: "
"O estado da base de datos de <systemitem role=\"package\">dpkg</systemitem> "
"pódese obter dende <filename>/var/lib/dpkg/status</filename>; tamén engada o "
"estado dos paquetes de <systemitem role=\"package\">apt</systemitem>, "
"indicado en <filename>/var/lib/apt/extended_states</filename>.  Debería ter "
"feito unha copia de seguridade antes de actualizar, tal como se indica en "
"<xref linkend=\"data-backup\"/>, pero tamén pode atopar copias de seguridade "
"de <filename>/var/lib/dpkg/status</filename> en <filename>/var/backups</"
"filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:98
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Os rexistros da sesión creados con <command>script</command>, tal como se "
"indica en <xref linkend=\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:104
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Os seus rexistros de <systemitem role=\"package\">apt</systemitem>, "
"dispoñibles en <filename>/var/log/apt/term.log</filename>; ou os rexistros "
"de <command>aptitude</command>, dispoñibles en <filename>/var/log/aptitude</"
"filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:113
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Debería revisar con calma e eliminar calquera información persoal e/ou "
"confidencial dos rexistros antes de incluílos no seu informe de fallos, "
"posto que a información publicarase nunha base de datos pública."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:122
msgid "Sources for this document"
msgstr "Fontes deste documento"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:124
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"As fontes deste documento están en formato DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. A versión HTML é "
"xerada con <systemitem role=\"package\">docbook-xsl</systemitem> e "
"<systemitem role=\"package\">xsltproc</systemitem>. A versión en PDF xérase "
"usando <systemitem role=\"package\">dblatex</systemitem> ou <systemitem role="
"\"package\">xmlroff</systemitem>. As fontes das Notas de Versión atópanse no "
"repositorio Git do <emphasis>Proxecto de Documentación Debian</emphasis>.  "
"Pode usar a <ulink url=\"&url-vcs-release-notes;\">interface na rede</ulink> "
"para acceder aos ficheiros individuais a través da rede e ver os seus "
"cambios.  Para máis información sobre como acceder a Git consulte as <ulink "
"url=\"&url-ddp-vcs-info;\">páxinas de información sobre SCV do Proxecto de "
"Documentación Debian</ulink>."
